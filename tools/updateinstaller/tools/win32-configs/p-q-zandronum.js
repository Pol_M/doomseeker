{
    "name" : "p-q-zandronum",
    "files" : [
        "engines/libzandronumq.dll",
        "translations/zandronumq_ca_ES.qm",
        "translations/zandronumq_es_ES.qm",
        "translations/zandronumq_pl_PL.qm"
    ],
    "updater-binary" : "updater.exe",
    "main-binary" : "doomseeker.exe"
}
