<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ca_ES">
<context>
    <name>FlagsPage</name>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="75"/>
        <source>Q-Zandronum</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="102"/>
        <source>Q-Zandronum 1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="104"/>
        <source>None</source>
        <translation>Res</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="105"/>
        <source>Old (ZDoom)</source>
        <translation>Vell (ZDoom)</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="106"/>
        <source>Hexen</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="107"/>
        <source>Strife</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="163"/>
        <source>Default</source>
        <translation>Per defecte</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="164"/>
        <source>No</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="165"/>
        <source>Yes</source>
        <translation>Si</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="275"/>
        <source>Unknown Q-Zandronum version in the config. Reverting to default.</source>
        <translation>Versió desconeguda de Q-Zandronum en la configuració. Revertint a la versió per defecte.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="302"/>
        <source>Tried to set unknown Q-Zandronum version. Reverting to default.</source>
        <translation>S&apos;ha intentat definir una versió desconeguda de Q-Zandronum. Revertint a la versió per defecte.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="14"/>
        <source>Form</source>
        <translation>Formulari</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="45"/>
        <source>Game version:</source>
        <translation>Versió del joc:</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="68"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;If this is checked then Q-Zandronum may override some of the settings selected here.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;(+sv_defaultdmflags)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Si això està marcat, llavors Q-Zandronum pot anul·lar algunes de les configuracions seleccionades aquí.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;(+sv_defaultdmflags)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="76"/>
        <source>Default DMFlags</source>
        <translation>DMFlags Per omissió</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="88"/>
        <location filename="../createserverdialogpages/flagspage.ui" line="678"/>
        <source>Environment</source>
        <translation>Entorn</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="99"/>
        <source>Falling damage:</source>
        <translation>Dany de caiguda:</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="130"/>
        <source>No monsters</source>
        <translation>Sense monstres</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="153"/>
        <source>Items respawn</source>
        <translation>Els articles reapareixen</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="160"/>
        <source>Barrels respawn</source>
        <translation>Els barrils reapareixen</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="167"/>
        <source>Respawn invulnerability and invisibility spheres.</source>
        <translation>Fer reaparèixer les esferes de invulnerabilitat i invisibilitat.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="170"/>
        <source>Mega powerups respawn</source>
        <translation>Mega potenciadors reapareixen</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="180"/>
        <source>Teams</source>
        <translation>Equips</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="186"/>
        <source>Server picks teams</source>
        <translation>El servidor tria equips</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="193"/>
        <source>Players can&apos;t switch teams</source>
        <translation>Els jugadors no poden canviar d&apos;equip</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="200"/>
        <source>Keep teams after a map change</source>
        <translation>Mantenir equips després d&apos;un canvi de mapa</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="223"/>
        <source>Hide allies on the automap</source>
        <translation>Amaga aliats al Automap</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="230"/>
        <source>Don&apos;t let players spy on allies</source>
        <translation>No permetre que els jugadors espiïn als seus aliats</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="253"/>
        <source>Instant flag/skull return</source>
        <translation>Retorn de bandera/calavera instantani</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="267"/>
        <source>Don&apos;t use ping-based backwards reconciliation for player-fired hitscans and rails.</source>
        <translation>No utilitzar reconciliació cap enrere basada en ping per hitscans i rails disparats per jugadors.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="270"/>
        <source>No unlagged</source>
        <translation>Sense unlagged</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="277"/>
        <source>Apply lmsspectatorsettings in all game modes.</source>
        <translation>Aplicar lmsspectatorsettings en tots els modes de joc.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="280"/>
        <source>Always apply LMS spectator settings</source>
        <translation>Sempre aplica la configuració d&apos;espectador LMS</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="287"/>
        <source>Enforces clients not to show medals, i.e. behave as if cl_medals == 0.</source>
        <translation>Obliga els clients a no mostrar medalles, és a dir, comportar-se com si cl_medals == 0.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="290"/>
        <source>No medals</source>
        <translation>Sense medalles</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="24"/>
        <source>General</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="321"/>
        <source>Disallow</source>
        <translation>No permetre</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="329"/>
        <source>Suicide</source>
        <translation>Suïcidi</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="336"/>
        <source>Respawn</source>
        <translation>Reaparèixer</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="343"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;P_RadiusAttack doesn&apos;t give players any z-momentum if the attack was made by a player. This essentially disables rocket jumping.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;P_RadiusAttack no dóna als jugadors cap z-momentum si l&apos;atac s&apos;ha fet per un jugador. Això essencialment desactiva el salt de coet.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="346"/>
        <source>Rocket jump</source>
        <translation>Salt de coet</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="353"/>
        <source>Taunt</source>
        <translation>Burlar-se</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="360"/>
        <source>Item drop</source>
        <translation>Deixar anar objecte</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="387"/>
        <source>Use automap</source>
        <translation>Utilitzar Automap</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="394"/>
        <source>Turn off translucency</source>
        <translation>Desactiva la translucidesa</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="401"/>
        <source>Use crosshairs</source>
        <translation>Utilitza punts de mira</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="408"/>
        <source>Use custom GL lighting settings</source>
        <translation>Utilitza configuracions d&apos;il·luminació GL personalitzades</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="415"/>
        <source>Enforces clients not to identify players, i.e. behave as if cl_identifytarget == 0.</source>
        <translation>Obliga els clients a no identificar jugadors, és a dir, comportar-se com si cl_identifytarget == 0.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="418"/>
        <source>Target identify</source>
        <translation>Identificar objectiu</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="425"/>
        <source>Enforces clients not to draw coop info, i.e. behave as if cl_drawcoopinfo == 0.</source>
        <translation>Obliga els clients a no dibuixar la informació del cooperatiu, és a dir, comportar-se com si cl_drawcoopinfo == 0.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="428"/>
        <source>Display coop info</source>
        <translation>Mostra informació cooperatiu</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="453"/>
        <source>Use autoaim</source>
        <translation>Usa autoapuntat</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="460"/>
        <source>Use freelook</source>
        <translation>Utilitza càmera lliure</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="467"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Players are not allowed to use the land CCMD. Because of Q-Zandronum&apos;s default amount of air control, flying players can get a huge speed boast with the land CCMD. Disallowing players to land, allows to keep the default air control most people are used to while not giving flying players too much of an advantage.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Els jugadors no poden usar el CCMD terrestre. A causa de la quantitat predeterminada de control en aire de Q-Zandronum, els jugadors que volen poden arribar a una gran velocitat amb el CCMD terrestre. Impedint que els jugadors aterrin, permet mantenir el control d&apos;aire predeterminat al qual la majoria de la gent està acostumada, mentre que no els dóna massa avantatge als jugadors que volen.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="470"/>
        <source>Use &apos;land&apos; console command</source>
        <translation>Utilitza la comanda de consola &apos;land&apos;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="477"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Don&apos;t allow players to change how strongly will their screen flash when they get hit.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;No permetre que els jugadors canviïn la intensitat del flaix quan reben un cop.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="480"/>
        <source>Change bloodied screen brightness</source>
        <translation>Canviar la brillantor de la pantalla ensagnada</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="492"/>
        <source>Abilities</source>
        <translation>Habilitats</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="503"/>
        <source>Jumping:</source>
        <translation>Saltar:</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="513"/>
        <source>Crouching:</source>
        <translation>Ajupir-se:</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="525"/>
        <source>Infinite inventory</source>
        <translation>Inventari infinit</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="532"/>
        <source>Infinite ammo</source>
        <translation>Munició infinita</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="539"/>
        <source>Like Quake 3</source>
        <translation>Com Quake 3</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="542"/>
        <source>Slowly lose health when over 100%</source>
        <translation>Perdre lentament la salut per sobre del 100%</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="549"/>
        <source>Can use chasecam</source>
        <translation>Pot usar la càmera de persecució</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="556"/>
        <source>Allow BFG freeaiming</source>
        <translation>Permet apuntat lliure de la BFG</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="566"/>
        <source>Behavior</source>
        <translation>Comportament</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="572"/>
        <source>Players can walk through each other</source>
        <translation>Els jugadors poden caminar entre ells</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="579"/>
        <source>Allies can walk through each other</source>
        <translation>Els aliats poden travessar-se entre si</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="586"/>
        <source>Players block each other normally</source>
        <translation>Els jugadors es bloquegen entre ells</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="596"/>
        <source>Don&apos;t check ammo when switching weapons</source>
        <translation>No comprovar munició quan es canvia d&apos;armes</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="608"/>
        <source>Force inactive players to spectate after:</source>
        <translation>Força als jugadors inactius al mode espectador després:</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="631"/>
        <source>min.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="313"/>
        <source>Players</source>
        <translation>Jugadors</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="671"/>
        <source>Score damage, not kills</source>
        <translation>Puntua el mal, no els assassinats</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="684"/>
        <source>Don&apos;t spawn Deathmatch weapons</source>
        <translation>No fer aparèixer armes de Deathmatch</translation>
    </message>
    <message>
        <source>Spawn map actors in coop as if the game was single player.</source>
        <translation type="vanished">Fer aparèixer a actors del mapa en cooperatiu com si el joc fós d&apos;un sol jugador.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="694"/>
        <source>Don&apos;t spawn any multiplayer actor in coop</source>
        <translation>No generis cap actor multijugador en el cooperatiu</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="701"/>
        <source>Monsters...</source>
        <translation>Monstres...</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="707"/>
        <source>are fast (like Nightmare)</source>
        <translation>són ràpids (com en Nightmare)</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="714"/>
        <source>respawn (like Nightmare)</source>
        <translation>reapareixen (com en Nightmare)</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="721"/>
        <source>must be killed to enable exit</source>
        <translation>han de ser assassinats per poder sortir</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="733"/>
        <source>Kill percentage:</source>
        <translation>Percentatge d&apos;assassinats:</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="753"/>
        <source>Multiplier of damage dealt by monsters.</source>
        <translation>Multiplicador del dany infligit pels monstres.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="756"/>
        <source>Damage factor:</source>
        <translation>Factor de Dany:</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="763"/>
        <source>&lt;html&gt;&lt;body&gt;&lt;p&gt;Multiplier of damage dealt by monsters.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;body&gt;&lt;p&gt;Multiplicador del dany infligit pels monstres.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="784"/>
        <source>Kill all monsters spawned by a boss cube when the boss dies</source>
        <translation>Mata a tots els monstres generats per un cub de &quot;boss&quot; quan el &quot;boss&quot; mor</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="806"/>
        <source>On player death...</source>
        <translation>Quant el jugador mor...</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="812"/>
        <source>respawn where died</source>
        <translation>reaparèixer on ha mort</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="819"/>
        <source>lose all inventory</source>
        <translation>perdre tot l&apos;inventari</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="826"/>
        <source>lose armor</source>
        <translation>perdre armadura</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="833"/>
        <source>lose keys</source>
        <translation>perdre claus</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="840"/>
        <source>lose powerups</source>
        <translation>perdre potenciadors</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="847"/>
        <source>lose weapons</source>
        <translation>perdre armes</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="854"/>
        <source>lose all ammo</source>
        <translation>perdre tota la munició</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="861"/>
        <source>lose half ammo</source>
        <translation>perdre la meitat de la munició</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Affects game modes where &amp;quot;max. lives&amp;quot; can be used. If set, players who become dead spectators (run out of lives) will still keep inventory in accordance to the &amp;quot;Lose *&amp;quot; flags above. If unset, players who lose all lives will always lose entire inventory.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Afecta les maneres de joc on &amp;quot;max. vides&amp;quot; pot ser usat. Si s&apos;estableix, els jugadors que es converteixin en espectadors morts (es quedin sense vida) seguiran mantenint l&apos;inventari d&apos;acord amb &amp;quot;Perdre *&amp;quot; indicadors amunt. Si no s&apos;estableix, els jugadors que perdin totes les vides perdran sempre l&apos;inventari complet.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="873"/>
        <source>Players who lose all lives can keep inventory</source>
        <translation>Els jugadors que perdin totes les vides poden conservar l&apos;inventari</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="885"/>
        <source>Share keys between players</source>
        <translation>Compartir claus entre jugadors</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="892"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Players will be respawned with full lives but the map will not be reset. Inventory will be preserved in accordance to &amp;quot;Lose inventory&amp;quot; flags. Players will be able to continue from the point where they died.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Els jugadors seran reapareguts amb vides completes però el mapa no es reiniciarà. L&apos;inventari es conservarà d&apos;acord amb els indicadors de &amp;quot;Perdre inventari&amp;quot;. Els jugadors podran continuar des del punt on van morir.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="895"/>
        <source>Survival only: no map reset when all players die</source>
        <translation>Només supervivència: no es restableix el mapa quan tots els jugadors moren</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="658"/>
        <source>Cooperative</source>
        <translation>Cooperatiu</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="922"/>
        <source>When players die, they...</source>
        <translation>Quan els jugadors moren, ells...</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="928"/>
        <source>respawn automatically</source>
        <translation>reaparèixer automàticament</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="935"/>
        <source>drop their weapon</source>
        <translation>deixar anar l&apos;arma</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="942"/>
        <source>respawn farthest away from others</source>
        <translation>reaparèixer el més lluny possible dels altres</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="949"/>
        <source>lose a frag</source>
        <translation>perdre un frag</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="956"/>
        <source>respawn with a shotgun</source>
        <translation>reaparèixer amb una escopeta</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="963"/>
        <source>don&apos;t get respawn protection</source>
        <translation>no obtenir protecció de reaparició</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="975"/>
        <source>When someone exits the level...</source>
        <translation>Quan algú surt del nivell ...</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="981"/>
        <source>continue to the next map</source>
        <translation>continuar al següent mapa</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="991"/>
        <source>restart the current level</source>
        <translation>reiniciar el nivell actual</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="998"/>
        <source>kill the player</source>
        <translation>mata al jugador</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1021"/>
        <source>Keep frags after map change</source>
        <translation>Mantenir frags després del canvi de mapa</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1033"/>
        <source>Weapons &amp;&amp; ammo</source>
        <translation>Armes i munició</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1039"/>
        <source>Weapons stay after pickup</source>
        <translation>Les armes romanen després de recollir-les</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1053"/>
        <source>Double ammo</source>
        <translation>Doble munició</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1063"/>
        <source>Don&apos;t spawn...</source>
        <translation>No fer aparèixer...</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1069"/>
        <source>health</source>
        <translation>salut</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1076"/>
        <source>armor</source>
        <translation>armadura</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1083"/>
        <source>runes</source>
        <translation>runes</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="916"/>
        <source>Deathmatch</source>
        <translation>Combat a mort</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1119"/>
        <source>Weapons</source>
        <translation>Armes</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1125"/>
        <source>Chainsaw</source>
        <translation>Serra mecànica</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1132"/>
        <source>Pistol</source>
        <translation>Pistola</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1139"/>
        <source>Shotgun</source>
        <translation>Escopeta</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1146"/>
        <source>Super shotgun</source>
        <translation>Súper escopeta</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1153"/>
        <source>Chaingun</source>
        <translation>Metralladora</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1160"/>
        <source>Minigun</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1167"/>
        <source>Rocket launcher</source>
        <translation>Llançacoets</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1174"/>
        <source>Grenade launcher</source>
        <translation>Llançagranades</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1181"/>
        <source>Plasma rifle</source>
        <translation>Rifle de plasma</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1188"/>
        <source>Railgun</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1208"/>
        <source>lmsallowedweapons:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1221"/>
        <location filename="../createserverdialogpages/flagspage.ui" line="1244"/>
        <location filename="../createserverdialogpages/flagspage.ui" line="1901"/>
        <location filename="../createserverdialogpages/flagspage.ui" line="1925"/>
        <location filename="../createserverdialogpages/flagspage.ui" line="1942"/>
        <location filename="../createserverdialogpages/flagspage.ui" line="1959"/>
        <location filename="../createserverdialogpages/flagspage.ui" line="1969"/>
        <location filename="../createserverdialogpages/flagspage.ui" line="1986"/>
        <source>0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1231"/>
        <source>lmsspectatorsettings:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1256"/>
        <source>Spectators can...</source>
        <translation>Els espectadors poden...</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1262"/>
        <source>talk to active players</source>
        <translation>parlar amb jugadors actius</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1269"/>
        <source>view the game</source>
        <translation>veure el joc</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1111"/>
        <source>LMS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1320"/>
        <source>Enable buggier wall clipping so players can wallrun.</source>
        <translation>Habilitar un atravessament de paret pitjor perquè els jugadors puguin fer wallrun.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1323"/>
        <source>Enable wall running</source>
        <translation>Activa wall running</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1330"/>
        <source>Pickups are only heard locally.</source>
        <translation>Les recollides d&apos;objectes només s&apos;escolten localment.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1333"/>
        <source>Don&apos;t let others hear pickups</source>
        <translation>No deixis que altres sentin la recollida d&apos;objectes</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1340"/>
        <source>Allow instant respawn</source>
        <translation>Permetre reaparició instantània</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1347"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Disable stealth monsters, since doom2.exe didn&apos;t have them.&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Note: this handles ZDoom&apos;s invisible monsters.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;THIS DOESN&apos;T AFFECT THE PARTIAL INVISIBILITY SPHERE IN ANY WAY. See &amp;quot;Monsters see semi-invisible players&amp;quot; for that.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Deshabilita els monstres silenciosos, ja que doom2.exe no els tenia.&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Nota: això fa anar els monstres invisibles de ZDoom.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;AIXÒ NO AFECTA L&apos;ESFERA DE INVISIBILITAT PARCIAL DE QUALSEVOL FORMA. Veure &amp;quot;Els monstres veuen jugadors semiinvisibles&amp;quot; per això.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1350"/>
        <source>Disable stealth monsters</source>
        <translation>Deshabilitar monstres silenciosos</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1357"/>
        <source>Limit actors to one sound at a time.</source>
        <translation>Limitar els actors a un so alhora.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1360"/>
        <source>Allow silent BFG trick</source>
        <translation>Permetre el truc de l&apos;BFG silenciós</translation>
    </message>
    <message>
        <source>Clients use the vanilla Doom weapon on pickup behavior.</source>
        <translation type="vanished">Els clients usen el comportament original de Doom en recollir d&apos;armes.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1370"/>
        <source>Original weapon switch</source>
        <translation>Canvi d&apos;armes original</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1377"/>
        <source>Limited movement in the air</source>
        <translation>Moviment limitat en l&apos;aire</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1384"/>
        <source>This affects the partial invisibility sphere.</source>
        <translation>Això afecta l&apos;esfera d&apos;invisibilitat parcial.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1387"/>
        <source>Monsters see semi-invisible players</source>
        <translation>Els monstres veuen els jugadors semi-invisibles</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1394"/>
        <source>Allow the map01 &quot;plasma bump&quot; bug.</source>
        <translation>Permetre error &quot;plasma bump&quot; al map01.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1397"/>
        <source>Plasma bump bug</source>
        <translation>Error plasma bump</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1404"/>
        <source>Any boss death activates map specials</source>
        <translation>Qualsevol mort de &quot;Boss&quot; activa especials del mapa</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1411"/>
        <source>Friction/pushers/pullers affect Monsters</source>
        <translation>Fricció/empentes/estrebades afecten els monstres</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1418"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Crushed monsters are turned into gibs, rather than replaced by gibs.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Els monstres aixafats esdevenen vísceres, en lloc de ser reemplaçats per aquestes.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1421"/>
        <source>Crusher gibs by morphing, not replacement</source>
        <translation>Aixafar transforma en vísceres, en lloc de reemplaçar per aquestes</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1428"/>
        <source>Block monster lines ignore friendly monsters</source>
        <translation>Les línies de bloqueig de monstres ignoren els monstres amistosos</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1435"/>
        <source>Find neighboring light level like Doom</source>
        <translation>Trobar el nivell de llum proper com en Doom</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1446"/>
        <source>Use doom2.exe&apos;s original intermission screens/music.</source>
        <translation>Utilitza la pantalla/música originals de l&apos;intermedi de missió de doom2.exe.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1449"/>
        <source>Use old intermission screens/music</source>
        <translation>Utilitza pantalla/música d&apos;intermedi de missió antigues</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1456"/>
        <source>Scrolling sectors are additive like in Boom.</source>
        <translation>Els sectors de desplaçament són additius com en Boom.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1459"/>
        <source>Scrolling sectors are additive</source>
        <translation>Els sectors de desplaçament són additius</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1466"/>
        <source>Sector sounds use original method for sound origin.</source>
        <translation>Els sons del sector usen el mètode original per a l&apos;origen del so.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1469"/>
        <source>Sector sounds use original method</source>
        <translation>Els sons del sector fan servir el mètode original</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1476"/>
        <source>Monsters cannot move when hanging over a dropoff.</source>
        <translation>Els monstres no es poden moure quan estan sobre un abisme.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1479"/>
        <source>No monsters dropoff move</source>
        <translation>Sense monstres moure en abisme</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1493"/>
        <source>Instantly moving floors are not silent.</source>
        <translation>Els pisos que es mouen a l&apos;instant no són silenciosos.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1496"/>
        <source>Instantly moving floors aren&apos;t silent</source>
        <translation>Els pisos que es mouen a l&apos;instant no són silenciosos</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1680"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;p&gt;Q-Zandronum uses more tracers to fill in the gaps, this reverts it to vanilla&apos;s 3 tracer behavior&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;p&gt;Q-Zandronum fa servir més traçadors per omplir els buits, això ho reverteix al comportament original de 3 traçadors&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Bring back vanilla ZDoom shooting below the crosshair.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Reverteix als trets de ZDoom &quot;vanilla&quot; sota la creueta.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Disable shooting where crosshair is</source>
        <translation type="vanished">Desactivar els trets on es troba la creueta</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1503"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Use Doom&apos;s random table instead of ZDoom&apos;s random number generator. Affects weapon damage among other things.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Utilitza la taula aleatòria de Doom en lloc del generador de nombres aleatoris de ZDoom. Afecta el dany de les armes entre altres coses.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1506"/>
        <source>Old random number generator</source>
        <translation>Antic generador de nombres aleatoris</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1486"/>
        <source>Monsters can&apos;t be pushed off cliffs</source>
        <translation>Els monstres no poden ser empesos a abismes</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1516"/>
        <source>Old damage radius (infinite height)</source>
        <translation>Ràdio de dany antic (alçada infinita)</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1523"/>
        <source>Minotaur&apos;s floor flame explodes immediately when feet are clipped</source>
        <translation>El foc del sòl del minotaure explota immediatament quan els peus el travessen</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1530"/>
        <source>Original velocity calc. for A_Mushroom in Dehacked</source>
        <translation>Càlcul de velocitat original per A_Mushroom a Dehacked</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1537"/>
        <source>Sprite sort order inverted for sprites of equal distance</source>
        <translation>Ordre de Sprites invertit per sprites en la mateixa distància</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1544"/>
        <source>Hitscans use original blockmap and hit check code</source>
        <translation>Els Hitscans usen el mapa de blocs original i el codi de verificació d&apos;impacte</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1551"/>
        <source>Draw polyobjects the old fashioned way</source>
        <translation>Dibuixar poliobjectes com als bons (i antics) temps</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1310"/>
        <source>Compatibility</source>
        <translation>Compatibilitat</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="691"/>
        <source>&lt;p&gt;Spawn map items, monsters and other objects in coop as if the game was single player.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Genera elements de mapes, monstres i altres objectes en mode cooperatiu com si el joc fos per a un sol jugador.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="791"/>
        <source>Don&apos;t count monsters in the &quot;end level&quot; sector towards kills</source>
        <translation>No comptis com a baixes els monstres al sector &quot;end level&quot;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="868"/>
        <source>&lt;p&gt;If set, players who lose all lives and become dead spectators will still keep inventory in accordance to the &amp;quot;Lose inventory&amp;quot; flags. When they will finally be able to play again, all their items will be returned to them.&lt;/p&gt;

&lt;p&gt;If unset, players who lose all lives will always lose their entire inventory, regardless of the &amp;quot;Lose inventory&amp;quot; flags.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Si s&apos;activa, els jugadors que es quedin sense vides mantindran el seu inventari d&apos;acord amb les flags de &amp;quot;Perdre inventari&amp;quot;. Quan ressuscitin, recuperaran els seus objectes.&lt;/p&gt;

&lt;p&gt;Si no, no se&apos;ls retornarà res, independentment de &amp;quot;Perdre inventari&amp;quot;.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1046"/>
        <source>Weapons always refill ammo</source>
        <translation>Les armes sempre proporcionen munició</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1367"/>
        <source>When a weapon is picked up, the weapon switch behaves like in vanilla Doom.</source>
        <translation>Quan una arma s&apos;agafa, es fa el canvi d&apos;armes del Doom vanilla.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1513"/>
        <source>&lt;p&gt;Shooting a rocket against a wall will damage the actors near this wall even if they are far below or above the explosion.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Disparar un coet a una paret farà mal a tots els actors que estan a prop, independentment de l&apos;altura.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1586"/>
        <location filename="../createserverdialogpages/flagspage.ui" line="1772"/>
        <source>&lt;p&gt;This allows for decorations to be pass-through for projectiles as they were originally in Doom.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Això permet que les decoracions es puguin travessar pels projectils, com al Doom original.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1589"/>
        <source>Use original missile clipping height</source>
        <translation>Utilitzar l&apos;alçada original d&apos;atravessament dels míssils</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1596"/>
        <source>Use sector based sound target code</source>
        <translation>Utilitza codi de so objectiu basat en sectors</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1603"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Doom&apos;s hitscan tracing code ignores all lines with both sides in the same sector. ZDoom&apos;s does not. This option reverts to the original but less precise behavior. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;El codi d&apos;escaneig d&apos;impactes de Doom ignora totes les línies amb banda i banda en el mateix sector. ZDoom no. Aquesta opció reverteix al comportament original però és menys precís. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1606"/>
        <source>Trace ignore lines w/ same sector on both sides</source>
        <translation>El traç ignora les línies amb el mateix sector a banda i banda</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1613"/>
        <source>Limit deh.MaxHealth to health bonus</source>
        <translation>Limitar deh.MaxHealth a la bonificació de salut</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1620"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The scrolling floor specials in Heretic and Hexen move the player much faster than the actual texture scrolling speed. Enable this option to restore this effect. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Els desplaçaments especials del sòl en Heretic i Hexen mouen al jugador molt més ràpid que la velocitat de desplaçament de la textura real. Habilita aquesta opció per restaurar aquest efecte.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1623"/>
        <source>Raven&apos;s scrollers use original speed</source>
        <translation>Els scrollers de Raven fan servir la seva velocitat original</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1630"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Add NOGRAVITY to actors named InvulnerabilitySphere, Soulsphere, Megasphere and BlurSphere when spawned by the map.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Afegiu NOGRAVITY als actors anomenats InvulnerabilitySphere, Soulsphere, Megasphere i BlurSphere quan apareixen pel mapa.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1633"/>
        <source>Add NOGRAVITY flag to spheres</source>
        <translation>Afegir l&apos;indicador NOGRAVITY a les esferes</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1640"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When a player leaves the game, don&apos;t stop any scripts of that player that are still running.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Quan un jugador abandona el joc, no aturar cap script que aquest jugador encara estigui executant.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1643"/>
        <source>Don&apos;t stop player scripts on disconnect</source>
        <translation>No aturar els scripts dels jugadors al desconnectar</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1650"/>
        <source>&lt;p&gt;If this is enabled, explosions cause a strong horizontal thrust like in old ZDoom versions.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Si això està habilitat, les explosions causen una forta empenta horitzontal com en versions antigues de ZDoom.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1653"/>
        <source>Old ZDoom horizontal thrust</source>
        <translation>Empenta horitzontal de l&apos;antic ZDoom</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1660"/>
        <source>&lt;p&gt;If this is enabled, non-SOLID things like flags fall through bridges (as they used to do in old ZDoom versions).&lt;/p&gt;</source>
        <translation>&lt;p&gt;Si això està habilitat, elements no SÒLIDS com banderes cauen a través de ponts (com solien fer en versions antigues de ZDoom).&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1663"/>
        <source>Old ZDoom bridge drops</source>
        <translation>Caigudes de pont de l&apos;antic ZDoom</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1670"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Uses old ZDoom jump physics, it&apos;s a minor bug in the gravity code that causes gravity application in the wrong place.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Fes servir l&apos;antiga física de salt ZDoom, és un error menor en el codi de gravetat que causa l&apos;aplicació de la gravetat en el lloc equivocat.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1673"/>
        <source>ZDoom 1.23B33 jump physics</source>
        <translation>Físiques de salt de ZDoom 1.23B33</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1683"/>
        <source>Use vanilla autoaim tracer behavior</source>
        <translation>Utilitza el comportament del traçador d&apos;autoapuntat original</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1690"/>
        <source>Ignore compositing when drawing masked midtextures</source>
        <translation>Ignorar la composició mentre es dibuixa textures mitjanes emmascarades</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1697"/>
        <source>It is impossible to face directly NSEW</source>
        <translation>És impossible mirar directament a l&apos;adreça cardinal</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1704"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Make weapons shoot where the crosshair is.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Fes que les armes disparin sempre a la creueta.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1707"/>
        <source>Enable shooting where crosshair is</source>
        <translation>Activar disparar sempre a la creueta</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1714"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Make projectile hitbox work across entire sprite instead of only upper half.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Fes que la hitbox del projectil funcioni a tot l&apos;sprite en lloc de només a la meitat superior.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1717"/>
        <source>Enable projectile hitbox fix</source>
        <translation>Activar correcció de la hitbox de projectil</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1728"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Use Doom&apos;s shortest texture find behavior. This is requied by some WADs in MAP07.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Utilitza el comportament de recerca de textura més curta de Doom. Això és requerit per alguns WADs a MAP07.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1731"/>
        <source>Find shortest textures like Doom</source>
        <translation>Troba les textures més curtes com en Doom</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1738"/>
        <source>Limit pain elementals to 20 lost souls</source>
        <translation>Limitar els elementals de dolor a 20 ànimes perdudes</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1745"/>
        <source>Spawned item drops on the floor</source>
        <translation>L&apos;element generat cau a terra</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1752"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Treat ACS scripts with the SCRIPTF_Net flag to be client side, i.e. executed on the clients, but not on the server.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Tractar els scripts ACS amb l&apos;indicador SCRIPTF_Net perquè s&apos;executin en el costat del client, és a dir, executats en els clients, però no al servidor.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1755"/>
        <source>NET scripts are clientside</source>
        <translation>Els scripts NET són del costat del client</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Don&apos;t execute predicted ACS scripts on client side. Turning this on can fix ACS teleports or jumppads on maps that are not using ACS_ExecuteWithResult.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;No executar els scripts ACS predictius al client. Activar això pot arreglar el teletransport ACS o els punts de salt en els mapes que no usen ACS_ExecuteWithResult.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Don&apos;t execute prediction ACS on client</source>
        <translation type="vanished">No executar les prediccions d&apos;ACS en el client</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1775"/>
        <source>Actors are infinitely tall</source>
        <translation>Els actors són infinitament alts</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1782"/>
        <source>Don&apos;t fix loop index for stair building.</source>
        <translation>No arreglar l&apos;índex del bucle per construir escales.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1785"/>
        <source>Use buggier stair building</source>
        <translation>Utilitza un constructor d&apos;escales amb més errors</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1792"/>
        <source>Disable Boom door light effect</source>
        <translation>Deshabilitar l&apos;efecte de llum de porta BOOM</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1799"/>
        <source>All special lines can drop use lines</source>
        <translation>Totes les línies especials poden deixar anar línies d&apos;ús</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1806"/>
        <source>Original sound curve</source>
        <translation>Corba de so original</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1813"/>
        <source>Disallow weapon change until fully drawn or hidden</source>
        <translation>No permetre el canvi d&apos;arma fins que estigui completament dibuixat o ocult</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1820"/>
        <source>West spawns are silent</source>
        <translation>Les aparicions &quot;West&quot; són silencioses</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1827"/>
        <source>Use the same floor motion behavior as Doom</source>
        <translation>Utilitza el comportament de moviment del pis com a Doom</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1576"/>
        <source>Compatibility 2</source>
        <translation>Compatibilitat 2</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1762"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When this option is ON, many Decorate and ACS functions will execute on clients without before receiving a command from the server. This can lead to smoother multiplayer, but can breaks mods that don&apos;t support it.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Quan estigui activat, moltes funcions Decorate i ACS s&apos;executaràn en els clients abans de rebre l&apos;ordre del servidor. Això pot portar a una experència multijugador més fluïda, però pot trencar algun mod que no soporti això.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1765"/>
        <source>Enable predicting Decorate and ACS functions on clients</source>
        <translation>Activar predicción de funciones Decorate y ACS en los clientes</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1834"/>
        <source>Disable wall friction</source>
        <translation>Desactivar fricció de paret</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1841"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The Quake thrust formula considers damage source and victim positions and applies thrust properly among all three axes.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;La fórmula d&apos;empenta de Quake considera la font del dany, la posició de la vícitima, i aplica l&apos;empenta en 3 dimensions.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1844"/>
        <source>Use Quake thrust formula</source>
        <translation>Usar la formula d&apos;empenta de Quake</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1851"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Fix cases when there is an elevated floor with a special, but an actor is standing on it&apos;s edge so that actor&apos;s center is in the air and the special doesn&apos;t trigger.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Arregla casos on hi ha un terra elevat amb &quot;especial&quot;, però un actor està en el seu extrem, així que el centre de l&apos;actor està en l&apos;aire i l&apos;&quot;especial&quot; no s&apos;activa.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1854"/>
        <source>Enable elevated sector special fix</source>
        <translation>Activa la correcció del especial en sector elevat</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1879"/>
        <source>Voting</source>
        <translation>Votacions</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1894"/>
        <source>q-zandronum dmflags</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1911"/>
        <source>dmflags</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1918"/>
        <source>dmflags2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1935"/>
        <source>q-zandronum compatflags</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1952"/>
        <source>compatflags</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1979"/>
        <source>compatflags2</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../zandronumqengineplugin.cpp" line="139"/>
        <source>Time limit:</source>
        <translation>Límit de temps:</translation>
    </message>
    <message>
        <location filename="../zandronumqengineplugin.cpp" line="147"/>
        <source>Frag limit:</source>
        <translation>Límit de frags:</translation>
    </message>
    <message>
        <location filename="../zandronumqengineplugin.cpp" line="158"/>
        <source>Point limit:</source>
        <translation>Límit de punts:</translation>
    </message>
    <message>
        <location filename="../zandronumqengineplugin.cpp" line="165"/>
        <source>Win limit:</source>
        <translation>Límit de victòries:</translation>
    </message>
    <message>
        <location filename="../zandronumqengineplugin.cpp" line="170"/>
        <source>Duel limit:</source>
        <translation>Límit de duels:</translation>
    </message>
    <message>
        <location filename="../zandronumqengineplugin.cpp" line="173"/>
        <source>Max. lives:</source>
        <translation>Max. vides:</translation>
    </message>
    <message>
        <location filename="../zandronumqserver.h" line="72"/>
        <source>&lt;&lt; Unknown &gt;&gt;</source>
        <translation>&lt;&lt; Desconegut &gt;&gt;</translation>
    </message>
</context>
<context>
    <name>VotingSetupWidget</name>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="14"/>
        <source>Form</source>
        <translation>Formulari</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="32"/>
        <source>Use this page</source>
        <translation>Utilitzeu aquesta pàgina</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="54"/>
        <source>Who can vote</source>
        <translation>Qui pot votar</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="60"/>
        <source>All can vote</source>
        <translation>Tots poden votar</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="70"/>
        <source>No one can vote</source>
        <translation>Ningú pot votar</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="77"/>
        <source>Spectators can&apos;t vote</source>
        <translation>Els espectadors no poden votar</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="102"/>
        <source>Minimum number of players required to call a vote:</source>
        <translation>Nombre mínim de jugadors necessaris per a sol·licitar una votació:</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="128"/>
        <source>Vote cooldown:</source>
        <translation>Temps entre vots:</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="149"/>
        <source>&lt;p&gt;Cooldown for the same type of vote is the double of this value. Setting this to 0 disables it.&lt;/p&gt;</source>
        <translation>&lt;p&gt;El temps és el doble per a vots del mateix tipus. Posar-ho a 0 ho desactiva.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="165"/>
        <source>min.</source>
        <translation>min.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="174"/>
        <source>Time before client can vote after connecting:</source>
        <translation>Temps abans no un nou client pugui votar:</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="208"/>
        <source>sec.</source>
        <translation>seg.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="219"/>
        <source>Allow specific votes</source>
        <translation>Permetre vots específics</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="295"/>
        <source>duellimit</source>
        <translation>límit de duel</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="305"/>
        <source>pointlimit</source>
        <translation>límit de punts</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="265"/>
        <source>timelimit</source>
        <translation>límit de temps</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="315"/>
        <source>winlimit</source>
        <translation>límit de victòries</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="235"/>
        <source>changemap</source>
        <translation>Canviar mapa</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="255"/>
        <source>nextmap</source>
        <translation>següent mapa</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="225"/>
        <source>kick</source>
        <translation>expulsar</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="245"/>
        <source>map</source>
        <translation>mapa</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="285"/>
        <source>fraglimit</source>
        <translation>límit de frags</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="275"/>
        <source>force players to spectate</source>
        <translation>obligar els jugadors al mode espectador</translation>
    </message>
    <message>
        <source>Vote flooding protection enabled</source>
        <translation type="vanished">Protecció contra inundacions de vots habilitada</translation>
    </message>
</context>
<context>
    <name>ZandronumqAboutProvider</name>
    <message>
        <location filename="../zandronumqgameinfo.cpp" line="110"/>
        <source>This plugin is distributed under the terms of the LGPL v2.1 or later.

</source>
        <translation>Aquest complement es distribueix sota els termes de la llicència LGPL v2.1 o posterior.

</translation>
    </message>
</context>
<context>
    <name>ZandronumqBroadcast</name>
    <message>
        <location filename="../zandronumqbroadcast.cpp" line="117"/>
        <source>Listening to Q-Zandronum&apos;s LAN servers broadcasts on port %1.</source>
        <translation>Escoltant les emissions de servidors LAN de Q-Zandronum al port %1.</translation>
    </message>
    <message>
        <location filename="../zandronumqbroadcast.cpp" line="123"/>
        <source>Failed to bind Q-Zandronum&apos;s LAN broadcasts listening socket on port %1. Will keep retrying silently.</source>
        <translation>Error en enllaçar les emissions dels servidors LAN de Q-Zandronum al port d&apos;escolta %1. Se seguirà intentant de fons.</translation>
    </message>
</context>
<context>
    <name>ZandronumqClientExeFile</name>
    <message>
        <source>client</source>
        <translation type="vanished">client</translation>
    </message>
</context>
<context>
    <name>ZandronumqGameInfo</name>
    <message>
        <location filename="../zandronumqgameinfo.cpp" line="37"/>
        <source>Time limit</source>
        <translation>Límit de temps</translation>
    </message>
    <message>
        <location filename="../zandronumqgameinfo.cpp" line="45"/>
        <source>Frag limit</source>
        <translation>Límit de frags</translation>
    </message>
    <message>
        <location filename="../zandronumqgameinfo.cpp" line="56"/>
        <source>Point limit</source>
        <translation>Límit de punts</translation>
    </message>
    <message>
        <location filename="../zandronumqgameinfo.cpp" line="63"/>
        <source>Win limit</source>
        <translation>Límit de victòries</translation>
    </message>
    <message>
        <location filename="../zandronumqgameinfo.cpp" line="68"/>
        <source>Duel limit</source>
        <translation>Límit de duels</translation>
    </message>
    <message>
        <location filename="../zandronumqgameinfo.cpp" line="71"/>
        <source>Max. lives</source>
        <translation>Max. vides</translation>
    </message>
    <message>
        <location filename="../zandronumqgameinfo.cpp" line="80"/>
        <source>Survival</source>
        <translation>Supervivència</translation>
    </message>
    <message>
        <location filename="../zandronumqgameinfo.cpp" line="81"/>
        <source>Invasion</source>
        <translation>Invasió</translation>
    </message>
    <message>
        <location filename="../zandronumqgameinfo.cpp" line="84"/>
        <source>Duel</source>
        <translation>Duel</translation>
    </message>
    <message>
        <location filename="../zandronumqgameinfo.cpp" line="85"/>
        <source>Terminator</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../zandronumqgameinfo.cpp" line="86"/>
        <source>LMS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../zandronumqgameinfo.cpp" line="87"/>
        <source>Team LMS</source>
        <translation>LMS per equips</translation>
    </message>
    <message>
        <location filename="../zandronumqgameinfo.cpp" line="88"/>
        <source>Possession</source>
        <translation>Possessió</translation>
    </message>
    <message>
        <location filename="../zandronumqgameinfo.cpp" line="89"/>
        <source>Team Poss</source>
        <translation>Possessió per equips</translation>
    </message>
    <message>
        <location filename="../zandronumqgameinfo.cpp" line="90"/>
        <source>Team Game</source>
        <translation>Joc per equips</translation>
    </message>
    <message>
        <location filename="../zandronumqgameinfo.cpp" line="92"/>
        <source>One Flag CTF</source>
        <translation>CTF amb una bandera</translation>
    </message>
    <message>
        <location filename="../zandronumqgameinfo.cpp" line="93"/>
        <source>Skulltag</source>
        <translation>Skulltag</translation>
    </message>
    <message>
        <location filename="../zandronumqgameinfo.cpp" line="94"/>
        <source>Domination</source>
        <translation>Dominació</translation>
    </message>
</context>
<context>
    <name>ZandronumqMasterClient</name>
    <message>
        <location filename="../zandronumqmasterclient.cpp" line="70"/>
        <source>You may contact Q-Zandronum staff about this through QC:DE Discord: &lt;b&gt;https://discord.com/invite/RN9hhmA&lt;/b&gt;</source>
        <translation>Pot posar-se en contacte amb el personal de Q-Zandronum a través d&apos;QC:DE Discord: &lt;b&gt;https://discord.com/invite/RN9hhmA&lt;/b&gt;</translation>
    </message>
</context>
<context>
    <name>ZandronumqRConProtocol</name>
    <message>
        <location filename="../zandronumqrconprotocol.cpp" line="69"/>
        <source>Connection attempt ...</source>
        <translation>Intent de connexió ...</translation>
    </message>
    <message>
        <location filename="../zandronumqrconprotocol.cpp" line="77"/>
        <source>Too many failed connection attempts. Aborting.</source>
        <translation>Massa intents fallits de connexió. Avortant.</translation>
    </message>
    <message>
        <location filename="../zandronumqrconprotocol.cpp" line="112"/>
        <source>Authenticating ...</source>
        <translation>Autenticant ...</translation>
    </message>
    <message>
        <location filename="../zandronumqrconprotocol.cpp" line="131"/>
        <source>Too many failed authentication attempts. Aborting.</source>
        <translation>Massa intents d&apos;autenticació fallits. Avortant.</translation>
    </message>
    <message numerus="yes">
        <location filename="../zandronumqrconprotocol.cpp" line="173"/>
        <source>Delaying for about %n seconds before next authentication attempt.</source>
        <translation>
            <numerusform>Retard d&apos;aproximadament %n segon abans del pròxim intent d&apos;autenticació.</numerusform>
            <numerusform>Retard d&apos;aproximadament %n segons abans del pròxim intent d&apos;autenticació.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../zandronumqrconprotocol.cpp" line="220"/>
        <source>Failed to establish connection.</source>
        <translation>Error en establir la connexió.</translation>
    </message>
    <message>
        <location filename="../zandronumqrconprotocol.cpp" line="223"/>
        <source>Timeout on authentication.</source>
        <translation>Límit de temps excedit en l&apos;autenticació.</translation>
    </message>
    <message>
        <location filename="../zandronumqrconprotocol.cpp" line="241"/>
        <source>You have been banned from this server.</source>
        <translation>Has estat expulsat d&apos;aquest servidor.</translation>
    </message>
    <message>
        <location filename="../zandronumqrconprotocol.cpp" line="246"/>
        <source>The protocol appears to be outdated.</source>
        <translation>El protocol sembla estar desactualitzat.</translation>
    </message>
    <message>
        <location filename="../zandronumqrconprotocol.cpp" line="284"/>
        <source>Authentication failure.</source>
        <translation>Error d’autenticació.</translation>
    </message>
    <message>
        <location filename="../zandronumqrconprotocol.cpp" line="289"/>
        <source>Remote console connection established.</source>
        <translation>Connexió a la consola remota establerta.</translation>
    </message>
    <message>
        <location filename="../zandronumqrconprotocol.cpp" line="290"/>
        <source>-----</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ZandronumqServer</name>
    <message>
        <location filename="../zandronumqserver.cpp" line="114"/>
        <source>Blue</source>
        <translation>Blau</translation>
    </message>
    <message>
        <location filename="../zandronumqserver.cpp" line="115"/>
        <source>Red</source>
        <translation>Vermell</translation>
    </message>
    <message>
        <location filename="../zandronumqserver.cpp" line="116"/>
        <source>Green</source>
        <translation>Verd</translation>
    </message>
    <message>
        <location filename="../zandronumqserver.cpp" line="117"/>
        <source>Gold</source>
        <translation>Or</translation>
    </message>
</context>
<context>
    <name>zandronumq1::Dmflags</name>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="40"/>
        <source>Use Doom&apos;s shortest texture behavior</source>
        <translation>Utilitza el comportament de textura més ràpid de Doom</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="42"/>
        <source>Don&apos;t fix loop index for stair building</source>
        <translation>No arreglar l&apos;índex del bucle per a la construcció d&apos;escales</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="44"/>
        <source>Pain elemental is limited to 20 lost souls</source>
        <translation>Pain elemental està limitat a 20 ànimes perdudes</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="46"/>
        <source>Pickups are only heard locally</source>
        <translation>Recollir articles només es poden escoltar localment</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="48"/>
        <source>Infinitely tall actors</source>
        <translation>Actors infinitament alts</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="50"/>
        <source>Limit actors to only one sound</source>
        <translation>Limitar els actors a un sol so</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="52"/>
        <source>Enable wallrunning</source>
        <translation>Habilitar wallrunning</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="54"/>
        <source>Dropped items spawn on floor</source>
        <translation>Els objectes deixats anar apareixen a terra</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="56"/>
        <source>Special lines block use line</source>
        <translation>Línies especials bloquegen l&apos;ús de línies</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="58"/>
        <source>Disable BOOM local door light effect</source>
        <translation>Deshabilitar l&apos;efecte de llum de porta local BOOM</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="60"/>
        <source>Raven&apos;s scrollers use their original speed</source>
        <translation>Els scrollers de Raven fan servir la seva velocitat original</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="62"/>
        <source>Use sector based sound target code</source>
        <translation>Utilitza codi de so objectiu basat en sectors</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="64"/>
        <source>Limit dehacked MaxHealth to health bonus</source>
        <translation>Limitar dehacked MaxHealth a la bonificació de vida</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="66"/>
        <source>Trace ignores lines with the same sector on both sides</source>
        <translation>El tracer ignora les línies amb el mateix sector a banda i banda</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="68"/>
        <source>Monsters can not move when hanging over a drop off</source>
        <translation>Els monstres no es poden moure quan estan sobre un abisme</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="70"/>
        <source>Scrolling sectors are additive like Boom</source>
        <translation>Els sectors de desplaçament són additius com Boom</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="72"/>
        <source>Monsters can see semi-invisible players</source>
        <translation>Els monstres poden veure jugadors semi-invisibles</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="74"/>
        <source>Instantly moving floors are not silent</source>
        <translation>Els pisos que es mouen a l&apos;instant no dir res</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="76"/>
        <source>Sector sounds use original method for sound origin</source>
        <translation>Els sons de sector usen el mètode original per a l&apos;origen del so</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="78"/>
        <source>Use original Doom heights for clipping against projectiles</source>
        <translation>Utilitzar altures originals de Doom durant una col·lisió amb míssils</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="80"/>
        <source>Monsters can&apos;t be pushed over dropoffs</source>
        <translation>Els monstres no poden ser empesos a abismes</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="82"/>
        <source>Any monster which calls BOSSDEATH counts for level specials</source>
        <translation>Qualsevol monstre que truqui a BOSSDEATH compten per a nivells especials</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="84"/>
        <source>Minotaur&apos;s floor flame is exploded immediately when feet are clipped</source>
        <translation>El foc del sòl del minotaure explota immediatament quan els peus el travessen</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="86"/>
        <source>Force original velocity calculations for A_Mushroom in Dehacked mods</source>
        <translation>Forçar càlculs de velocitat originals per A_Mushroom a mods Dehacked</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="88"/>
        <source>Monsters are affected by friction and pushers/pullers</source>
        <translation>Els monstres es veuen afectats per la fricció i empentes/estrebades</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="90"/>
        <source>Crushed monsters are turned into gibs, rather than replaced by gibs</source>
        <translation>Els monstres aixafats esdevenen vísceres, en lloc de ser reemplaçats per aquestes</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="92"/>
        <source>Friendly monsters aren&apos;t blocked by monster-blocking lines</source>
        <translation>Els monstres amistosos ignoren les línies de bloqueig de monstres</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="94"/>
        <source>Invert sprite sorting order for sprites of equal distance</source>
        <translation>Invertir ordre de classificació de sprites per sprites d&apos;igual distància</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="96"/>
        <source>Hitscans use original blockmap and hit check code</source>
        <translation>Els Hitscans usen el mapa de blocs original i el codi de verificació d&apos;impacte</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="98"/>
        <source>Find neighboring light level like like Doom</source>
        <translation>Busqui el nivell de llum veïna com en Doom</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="100"/>
        <source>Draw polyobjects the old fashioned way</source>
        <translation>Dibuixar poliobjectes com als bons (i antics) temps</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="102"/>
        <source>Ignore compositing when drawing masked midtextures</source>
        <translation>Ignorar la composició mentre es dibuixa textures mitjanes emmascarades</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="110"/>
        <source>It is impossible to directly face cardinal direction</source>
        <translation>És impossible mirar directament la direcció cardinal</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="112"/>
        <source>Use the same floor motion behavior as Doom</source>
        <translation>Utilitza el comportament de moviment del pis com a Doom</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="122"/>
        <source>Net scripts are client side</source>
        <translation>Els scripts NET s&apos;executen en el client</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="124"/>
        <source>Don&apos;t execute prediction ACS on client</source>
        <translation>No executar prediccions d&apos;ACS en el client</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="126"/>
        <source>Players can&apos;t use &apos;land&apos; CCMD</source>
        <translation>Els jugadors no poden usar el comandament &apos;land&apos;</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="129"/>
        <source>Use Doom&apos;s original random number generator</source>
        <translation>Utilitzar el generador de xifres aleatòries original de Doom</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="131"/>
        <source>Spheres have NOGRAVITY flag</source>
        <translation>Les esferes tenen l&apos;indicador NOGRAVITY</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="134"/>
        <source>Don&apos;t stop player scripts on disconnect</source>
        <translation>No aturar els scripts dels jugadors al desconnectar</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="137"/>
        <source>Use horizontal explosion thrust of old ZDoom versions</source>
        <translation>Utilitzar l&apos;empenta d&apos;explosió horitzontal de versions antigues de ZDoom</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="140"/>
        <source>Non-SOLID things fall through invisible bridges</source>
        <translation>Coses no sòlides cauen a través de ponts invisibles</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="142"/>
        <source>Use old ZDoom jump physics</source>
        <translation>Fes servir físiques velles de salt de ZDoom</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="145"/>
        <source>Disallow weapon change when in mid raise/lower</source>
        <translation>No permetre el canvi d&apos;arma quan està a mig pujar/baixar</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="147"/>
        <source>Use vanilla&apos;s autoaim tracer behavior</source>
        <translation>Utilitzar el comportament de autoapuntat original</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="149"/>
        <source>West spawns are silent</source>
        <translation>Les aparicions &quot;West&quot; són silencioses</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="151"/>
        <source>Limited movement in the air</source>
        <translation>Moviment limitat en l&apos;aire</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="153"/>
        <source>Allow map01 &quot;plasma bump&quot; bug</source>
        <translation>Permetre error &quot;plasma bump&quot; al map01</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="155"/>
        <source>Allow instant respawn after death</source>
        <translation>Permetre reaparició instantània després de morir</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="157"/>
        <source>Disable taunting</source>
        <translation>Desactivar burles</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="159"/>
        <source>Use doom2.exe&apos;s original sound curve</source>
        <translation>Utilitza la corba sonora original de doom2.exe</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="161"/>
        <source>Use original doom2 intermission music</source>
        <translation>Utilitzar la música original d&apos;intermedi de doom2</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="163"/>
        <source>Disable stealth monsters</source>
        <translation>Deshabilitar monstres silenciosos</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="165"/>
        <source>Radius damage has infinite height</source>
        <translation>El radi de dany té una alçada infinita</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="167"/>
        <source>Disable crosshair</source>
        <translation>Desactivar espiell</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="169"/>
        <source>Force weapon switch</source>
        <translation>Forçar el canvi d&apos;arma</translation>
    </message>
    <message>
        <source>Disable shooting where crosshair is</source>
        <translation type="vanished">Desactivar els tirs on es troba la creueta</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="346"/>
        <source>Disable wall friction</source>
        <translation>Desactivar fricció de paret</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="177"/>
        <source>Do not spawn health items (DM)</source>
        <translation>No fer aparèixer ítems de salut (DM)</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="179"/>
        <source>Do not spawn powerups (DM)</source>
        <translation>No fer aparèixer potenciadors (DM)</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="181"/>
        <source>Weapons remain after pickup (DM)</source>
        <translation>Les armes romanen després de recollir-les (DM)</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="183"/>
        <source>Falling damage (old ZDoom)</source>
        <translation>Dany de caiguda (antic ZDoom)</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="185"/>
        <source>Falling damage (Hexen)</source>
        <translation>Dany de caiguda (Hexen)</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="187"/>
        <source>Falling damage (Strife)</source>
        <translation>Dany de caiguda (Strife)</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="189"/>
        <source>Stay on same map when someone exits (DM)</source>
        <translation>Romandre al mapa quan algú ho acabi (DM)</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="191"/>
        <source>Spawn players as far as possible (DM)</source>
        <translation>Fer aparèixer els jugadors el més lluny possible (DM)</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="193"/>
        <source>Automatically respawn dead players (DM)</source>
        <translation>Reaparició automàtica de jugadors morts (DM)</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="195"/>
        <source>Don&apos;t spawn armor (DM)</source>
        <translation>No fer aparèixer armadura (DM)</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="197"/>
        <source>Kill anyone who tries to exit the level (DM)</source>
        <translation>Mata a qualsevol que intenti acabar el nivell (DM)</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="199"/>
        <source>Infinite ammo</source>
        <translation>Munició infinita</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="201"/>
        <source>No monsters</source>
        <translation>Sense monstres</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="203"/>
        <source>Monsters respawn</source>
        <translation>Els monstres reapareixen</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="205"/>
        <source>Items other than invuln. and invis. respawn</source>
        <translation>Els objectes (a excepció d&apos;invulnerabilitat i invisibilitat) reapareixen</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="207"/>
        <source>Fast monsters</source>
        <translation>Monstres ràpids</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="209"/>
        <source>No jumping</source>
        <translation>No salts</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="211"/>
        <source>No freelook</source>
        <translation>No càmera lliure</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="213"/>
        <source>Respawn invulnerability and invisibility</source>
        <translation>Reaparició d&apos;invulnerabilitat i invisibilitat</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="215"/>
        <source>No multiplayer weapons in cooperative</source>
        <translation>Sense armes multijugador en cooperatiu</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="217"/>
        <source>No crouching</source>
        <translation>Sense ajupir-se</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="219"/>
        <source>Lose all old inventory on respawn (COOP)</source>
        <translation>Perdre tot l&apos;inventari al reaparèixer (COOP)</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="221"/>
        <source>Lose keys on respawn (COOP)</source>
        <translation>Perdre claus en reaparèixer (COOP)</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="223"/>
        <source>Lose weapons on respawn (COOP)</source>
        <translation>Perdre armes en reaparèixer (COOP)</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="225"/>
        <source>Lose armor on respawn (COOP)</source>
        <translation>Perdre armadura a reaparèixer (COOP)</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="227"/>
        <source>Lose powerups on respawn (COOP)</source>
        <translation>Perdre potenciadors en reaparèixer (COOP)</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="229"/>
        <source>Lose ammo on respawn (COOP)</source>
        <translation>Perdre munició en reaparèixer (COOP)</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="231"/>
        <source>Lose half your ammo on respawn (COOP)</source>
        <translation>Perdre la meitat de la munició al reaparèixer (COOP)</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="233"/>
        <source>Jumping allowed</source>
        <translation>Saltar permès</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="235"/>
        <source>Crouching allowed</source>
        <translation>Ajupir-se permès</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="243"/>
        <source>Drop weapons upon death</source>
        <translation>Deixar anar armes en morir</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="245"/>
        <source>Don&apos;t spawn runes</source>
        <translation>No generar runes</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="247"/>
        <source>Instantly return flags (ST/CTF)</source>
        <translation>Retornar banderes automàticament (ST/CTF)</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="249"/>
        <source>Don&apos;t allow players to switch teams</source>
        <translation>No permetre que els jugadors canviïn d&apos;equip</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="251"/>
        <source>Players are automatically assigned teams</source>
        <translation>Als jugadors se&apos;ls assignen equips automàticament</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="253"/>
        <source>Double the amount of ammo given</source>
        <translation>Duplicar la quantitat de munició</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="255"/>
        <source>Players slowly lose health over 100% like Quake</source>
        <translation>Els jugadors perden lentament la salut per sobre del 100% com en Quake</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="257"/>
        <source>Allow BFG freeaiming</source>
        <translation>Permet apuntat lliure de la BFG</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="259"/>
        <source>Barrels respawn</source>
        <translation>Els barrils reapareixen</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="261"/>
        <source>No respawn protection</source>
        <translation>No hi ha protecció de reaparició</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="263"/>
        <source>All players start with a shotgun</source>
        <translation>Tots els jugadors comencen amb una escopeta</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="265"/>
        <source>Players respawn where they died (COOP)</source>
        <translation>Els jugadors reapareixen on van morir (COOP)</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="267"/>
        <source>Don&apos;t clear frags after each level</source>
        <translation>No esborrar indicadors després de cada nivell</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="269"/>
        <source>Player can&apos;t respawn</source>
        <translation>El jugador no pot reaparèixer</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="271"/>
        <source>Lose a frag when killed</source>
        <translation>Perdre un frag en morir</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="273"/>
        <source>Infinite inventory</source>
        <translation>Inventari infinit</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="275"/>
        <source>All monsters must be killed before exiting</source>
        <translation>Tots els monstres han de ser assassinats abans de sortir</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="277"/>
        <source>Players can&apos;t see the automap</source>
        <translation>Els jugadors no poden veure el Automap</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="279"/>
        <source>Allies can&apos;t be seen on the automap</source>
        <translation>Els aliats no es poden veure al Automap</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="281"/>
        <source>You can&apos;t spy allies</source>
        <translation>No pots espiar als teus aliats</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="283"/>
        <source>Players can use chase cam</source>
        <translation>Els jugadors poden fer servir la càmera de persecució</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="285"/>
        <source>Players can&apos;t suicide</source>
        <translation>Els jugadors no poden suïcidar-se</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="287"/>
        <source>Players can&apos;t use autoaim</source>
        <translation>Els jugadors no poden usar autoapuntat</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="289"/>
        <source>Don&apos;t check ammo when switching weapons</source>
        <translation>No comprovar munició quan es canvia d&apos;armes</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="291"/>
        <source>Kill all monsters spawned by a boss cube when the boss dies</source>
        <translation>Mata a tots els monstres generats per un cub de &quot;boss&quot; quan el &quot;boss&quot; mor</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="294"/>
        <source>Do not count monsters in &apos;end level when dying&apos; sectors towards kill count</source>
        <translation>No compti els assassinats de monstres en els sectors de &apos;fi de nivell en cas de mort&apos;</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="296"/>
        <source>Weapons always refill ammo (DM)</source>
        <translation>Les armes sempre proporcionen munició (DM)</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="306"/>
        <source>Clients can&apos;t identify targets</source>
        <translation>Els clients no poden identificar els objectius</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="309"/>
        <source>lmsspectatorsettings applied in all game modes</source>
        <translation>Apliqui lmsspectatorsettings en tots els modes de joc</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="311"/>
        <source>Clients can&apos;t draw coop info</source>
        <translation>Els clients no poden usar el CVar &quot;Coop Info&quot;</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="313"/>
        <source>Unlagged is disabled</source>
        <translation>Unlagged està deshabilitat</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="315"/>
        <source>Players don&apos;t block each other</source>
        <translation>Els jugadors no es bloquegen entre ells</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="317"/>
        <source>Clients don&apos;t show medals</source>
        <translation>Els clients no mostren medalles</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="319"/>
        <source>Keys are shared between players</source>
        <translation>Les claus es comparteixen entre jugadors</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="321"/>
        <source>Player teams are preserved between maps</source>
        <translation>Els equips es conserven entre mapes</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="323"/>
        <source>Force OpenGL defaults</source>
        <translation>Força els valors predeterminats d&apos;OpenGL</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="325"/>
        <source>No rocket jumping</source>
        <translation>Sense &quot;saltar coets&quot;</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="327"/>
        <source>Award damage instead of kills</source>
        <translation>Premiar dany en lloc d&apos;assassinats</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="329"/>
        <source>Force drawing alpha</source>
        <translation>Força dibuixant alfa</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="331"/>
        <source>Don&apos;t spawn multiplayer things</source>
        <translation>No fer aparèixer coses del multijugador</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="334"/>
        <source>Force blood screen brightness on clients to emulate vanilla</source>
        <translation>Força la brillantor de la sang en pantalla en els clients per emular l&apos;original</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="336"/>
        <source>Teammates don&apos;t block each other</source>
        <translation>Els companys d&apos;equip no es bloquegen entre ells</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="338"/>
        <source>No dropping allowed</source>
        <translation>No es permet deixar anar articles</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="340"/>
        <source>No map reset on death in survival</source>
        <translation>No es restableix el mapa al morir en supervivència</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="342"/>
        <source>Dead players can keep inventory</source>
        <translation>Els jugadors morts poden conservar l&apos;inventari</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="344"/>
        <source>Enable shooting where crosshair is</source>
        <translation>Activar disparar sempre a la creueta</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="348"/>
        <source>Enable projectile hitbox fix</source>
        <translation>Activar correcció de la hitbox de projectil</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="350"/>
        <source>Use Quake thrust formula</source>
        <translation>Usar la formula d&apos;empenta de Quake</translation>
    </message>
    <message>
        <location filename="../zandronumq1dmflags.cpp" line="352"/>
        <source>Enable elevated sector special fix</source>
        <translation>Activa la correcció del especial en sector elevat</translation>
    </message>
</context>
</TS>
