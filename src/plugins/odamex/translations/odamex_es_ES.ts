<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_ES">
<context>
    <name>OdamexAboutProvider</name>
    <message>
        <location filename="../odamexengineplugin.cpp" line="49"/>
        <source>This plugin is distributed under the terms of the LGPL v2.1 or later.

</source>
        <translation>Este complemento se distribuye bajo los términos de la licencia LGPL v2.1 o posterior.

</translation>
    </message>
</context>
<context>
    <name>OdamexGameInfo</name>
    <message>
        <source>Duel</source>
        <translation type="vanished">Duelo</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="28"/>
        <source>Items respawn</source>
        <translation>Objetos reaparecen</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="29"/>
        <source>Weapons stay</source>
        <translation>Armas permanecen</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="30"/>
        <source>Friendly fire</source>
        <translation>Fuego amigo</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="31"/>
        <source>Allow exit</source>
        <translation>Permitir salir</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="32"/>
        <source>Infinite ammo</source>
        <translation>Munición infinita</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="33"/>
        <source>No monsters</source>
        <translation>Sin monstruos</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="34"/>
        <source>Monsters respawn</source>
        <translation>Monstruos reaparecen</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="35"/>
        <source>Fast monsters</source>
        <translation>Monstruos rápidos</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="36"/>
        <source>Jumping allowed</source>
        <translation>Permitir saltar</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="37"/>
        <source>Freelook allowed</source>
        <translation>Cámara libre permitida</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="38"/>
        <source>Wad can be downloaded</source>
        <translation>El Wad se puede descargar</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="39"/>
        <source>Server resets on empty</source>
        <translation>El servidor se restablece cuando está vacío</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="40"/>
        <source>Clean Maps</source>
        <translation>Mapas Límpios</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="42"/>
        <source>Kill anyone who tries to leave the level</source>
        <translation>Mata a quien intente abandonar el nivel</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="52"/>
        <source>Lives</source>
        <translation>Vidas</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="53"/>
        <source>Teams</source>
        <translation>Equipos</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="54"/>
        <source>Attack &amp;&amp; Defend</source>
        <translation>Atacar y defender</translation>
    </message>
</context>
<context>
    <name>OdamexGameMode</name>
    <message>
        <location filename="../odamexgamemode.cpp" line="34"/>
        <location filename="../odamexgamemode.cpp" line="54"/>
        <source>Horde</source>
        <translation>Horda</translation>
    </message>
    <message>
        <location filename="../odamexgamemode.cpp" line="48"/>
        <source>Duel</source>
        <translation>Duelo</translation>
    </message>
    <message>
        <location filename="../odamexgamemode.cpp" line="49"/>
        <source>Survival</source>
        <translation>Supervivencia</translation>
    </message>
    <message>
        <location filename="../odamexgamemode.cpp" line="50"/>
        <source>Last Marine Standing</source>
        <translation>Último marine en pie</translation>
    </message>
    <message>
        <location filename="../odamexgamemode.cpp" line="51"/>
        <source>Teams Last Marine Standing</source>
        <translation>Último marine en pie (Equipos)</translation>
    </message>
    <message>
        <location filename="../odamexgamemode.cpp" line="52"/>
        <source>Attack &amp; Defend CTF</source>
        <translation>Atacar y defender (CTF)</translation>
    </message>
    <message>
        <location filename="../odamexgamemode.cpp" line="53"/>
        <source>LMS Capture The Flag</source>
        <translation>Último marine en pie (CTF)</translation>
    </message>
    <message>
        <location filename="../odamexgamemode.cpp" line="55"/>
        <source>Survival Horde</source>
        <translation>Supervivencia de hordas</translation>
    </message>
</context>
</TS>
