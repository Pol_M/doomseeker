//------------------------------------------------------------------------------
// datetime.cpp
//------------------------------------------------------------------------------
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// 02110-1301  USA
//
//------------------------------------------------------------------------------
// Copyright (C) 2024 "Zalewa" <zalewapl@gmail.com>
//------------------------------------------------------------------------------
#include "datetime.h"

QString DateTime::toISO8601(const QDateTime &datetime)
{
	// https://stackoverflow.com/questions/21976264/qt-isodate-formatted-date-time-including-timezone
	return datetime.toOffsetFromUtc(datetime.offsetFromUtc()).toString(Qt::ISODate);
}

QString DateTime::toPathFriendlyUTCISO8601(const QDateTime &datetime)
{
	return datetime.toUTC().toString("yyyy-MM-ddThhmmssZ");
}

QDateTime DateTime::fromPathFriendlyUTCISO8601(const QString &datetime)
{
	auto result = QDateTime::fromString(datetime, "yyyy-MM-ddThhmmssZ");
	if (result.isValid())
		result.setTimeSpec(Qt::UTC);
	return result;
}
